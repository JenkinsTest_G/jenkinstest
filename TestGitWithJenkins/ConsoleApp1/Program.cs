﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
   public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(PrintNumbers());
        }
        public static string PrintHelloWorld()
        {
            return "Hello World";
        }
        public static bool PrintNumbers()
        {
            try
            {
                Console.WriteLine("entered random numbers");
                int[] num = new int[5] { 11, 22, 31, 24, 35 };
                Console.WriteLine("the entered number in ascending order");
                var res = num.ToList().OrderByDescending(a => a);
                var res1 = res.Reverse().ToList();
                for (int i = 0; i < res1.Count; i++)
                {
                    Console.WriteLine(res1[i]);
                }
                Console.ReadLine();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            return true;
        }
    }
}
